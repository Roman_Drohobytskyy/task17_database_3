DELIMITER //
CREATE
TRIGGER pharmacy_id_on_delete
BEFORE DELETE
ON pharmacy FOR EACH ROW
BEGIN
	IF (old.id IN (SELECT pharmacy_id FROM employee))
		THEN SIGNAL SQLSTATE '45000' 
		SET MESSAGE_TEXT = "Error! There is at least one employee in this pharmacy.";
	END IF;
    
	IF (old.id IN (SELECT pharmacy_id FROM pharmacy_medicine))
		THEN SIGNAL SQLSTATE '45000' 
		SET MESSAGE_TEXT = "Error! There is at least one medicine in this pharmacy.";
	END IF;
END //

CREATE
TRIGGER medicine_id_on_delete
BEFORE DELETE
ON medicine FOR EACH ROW
BEGIN
	IF (old.id IN (SELECT medicine_id FROM pharmacy_medicine))
		THEN SIGNAL SQLSTATE '45000' 
		SET MESSAGE_TEXT = "Error! There is at least one pharmacy containing this medicine.";
	END IF;
    
	IF (old.id IN (SELECT medicine_id FROM medicine_zone))
		THEN SIGNAL SQLSTATE '45000' 
		SET MESSAGE_TEXT = "Error! There is at least one zone with this medicine.";
	END IF;
END //

CREATE
TRIGGER post_on_delete
BEFORE DELETE
ON post FOR EACH ROW
BEGIN
	IF (old.post IN (SELECT post FROM employee))
		THEN SIGNAL SQLSTATE '45000' 
		SET MESSAGE_TEXT = "Error! There is at least one employee with this post.";
	END IF;
END //

CREATE
TRIGGER street_on_delete
BEFORE DELETE
ON street FOR EACH ROW
BEGIN
	IF (old.street IN (SELECT street FROM farmacy))
		THEN SIGNAL SQLSTATE '45000' 
		SET MESSAGE_TEXT = "Error! There is at least one pharmacy on this street.";
	END IF;
END //

CREATE
TRIGGER zone_on_delete
BEFORE DELETE
ON zone FOR EACH ROW
BEGIN
	IF (old.id IN (SELECT zone_id FROM medicine_zone))
		THEN SIGNAL SQLSTATE '45000' 
		SET MESSAGE_TEXT = "Error! There is at least one medicine from this zone.";
	END IF;
END //
DELIMITER ;