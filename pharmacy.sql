DELIMITER //
CREATE
TRIGGER pharmacy_id_foreign
BEFORE INSERT
ON employee FOR EACH ROW
BEGIN
	IF (new.pharmacy_id NOT IN (SELECT id FROM pharmacy))
		THEN SIGNAL SQLSTATE '45000' 
		SET MESSAGE_TEXT = "This key doesn't exist in table pharmacy";
	END IF;
END //

CREATE
TRIGGER post_id_foreign
BEFORE INSERT
ON employee FOR EACH ROW
BEGIN
	IF (new.post NOT IN (SELECT post FROM post))
		THEN SIGNAL SQLSTATE '45000' 
		SET MESSAGE_TEXT = "This key doesn't exist in table post";
	END IF;
END //

CREATE
TRIGGER pharmacy_id_foreign_in_pharmacy
BEFORE INSERT
ON pharmacy FOR EACH ROW
BEGIN
	IF (new.street NOT IN (SELECT street FROM street))
		THEN SIGNAL SQLSTATE '45000' 
		SET MESSAGE_TEXT = "This key doesn't exist in table street";
	END IF;
END //

CREATE
TRIGGER pharmacy_id_foreign_in_pharmacy_medicine
BEFORE INSERT
ON pharmacy_medicine FOR EACH ROW
BEGIN
	IF (new.pharmacy_id NOT IN (SELECT id FROM pharmacy))
		THEN SIGNAL SQLSTATE '45000' 
		SET MESSAGE_TEXT = "This key doesn't exist in table pharmacy";
	END IF;
END //

CREATE
TRIGGER medicine_id_foreign_in_pharmacy_medicine
BEFORE INSERT
ON pharmacy_medicine FOR EACH ROW
BEGIN
	IF (new.medicine_id NOT IN (SELECT id FROM medicine))
		THEN SIGNAL SQLSTATE '45000' 
		SET MESSAGE_TEXT = "This key doesn't exist in table medicine";
	END IF;
END //

CREATE
TRIGGER medicine_id_foreign_in_medicine_zone
BEFORE INSERT
ON medicine_zone FOR EACH ROW
BEGIN
	IF (new.medicine_id NOT IN (SELECT id FROM medicine))
		THEN SIGNAL SQLSTATE '45000' 
		SET MESSAGE_TEXT = "This key doesn't exist in table medicine";
	END IF;
END //

CREATE
TRIGGER zone_id_foreign_in_medicine_zone
BEFORE INSERT
ON medicine_zone FOR EACH ROW
BEGIN
	IF (new.zone_id NOT IN (SELECT id FROM zone))
		THEN SIGNAL SQLSTATE '45000' 
		SET MESSAGE_TEXT = "This key doesn't exist in table medicine";
	END IF;
END //

DELIMITER ;