DELIMITER //
CREATE
TRIGGER pharmacy_id_on_delete
BEFORE DELETE
ON pharmacy FOR EACH ROW
BEGIN
	IF (old.id IN (SELECT pharmacy_id FROM employee))
		THEN SIGNAL SQLSTATE '45000' 
		SET MESSAGE_TEXT = "Error! There is at least one employee in this pharmacy.";
	END IF;
    
	IF (old.id IN (SELECT pharmacy_id FROM pharmacy_medicine))
		THEN SIGNAL SQLSTATE '45000' 
		SET MESSAGE_TEXT = "Error! There is at least one medicine in this pharmacy.";
	END IF;
END //

DELIMITER ;